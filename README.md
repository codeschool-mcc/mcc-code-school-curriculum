# README #

This README would normally document whatever steps are necessary to get your application up and running.

## TODO
- [X] Add link to assignment on Sharepoint
- [X] Add link to Sharepoint folder for the date

## Data dictionary
* `dateLink` is the SharePoint link to the folder containing the student prep plan and assignments for a given day. The SharePoint link settings are "People with existing access"
* `nameLink` is the SharePoint link to the assignment Word document. The SharePoint link settings are "People with existing access"

## Classes and Dates added
### Program Increment 1
- [x] 09/09/2019
- [x] 09/11/2019
- [ ] 09/16/2019
- [ ] 09/18/2019
- [x] 09/23/2019
- [ ] 09/25/2019
- [ ] 09/30/2019
- [ ] 09/30/2019
- [ ] 10/02/2019
- [ ] 10/07/2019
- [ ] 10/09/2019
- [ ] 10/14/2019
- [ ] 10/16/2019
- [ ] 10/21/2019

### Program Increment 2
- [x] 11/11/2019
- [x] 11/13/2019
- [ ] 11/16/2019
- [ ] 11/18/2019
- [ ] 11/20/2019
- [ ] 11/25/2019
- [ ] 12/02/2019
- [ ] 12/04/2019
- [ ] 12/09/2019
- [ ] 12/11/2019
- [ ] 12/16/2019
- [ ] 12/18/2019
- [ ] 12/23/2019
- [ ] 01/04/2020
- [ ] 01/06/2020
- [ ] 01/08/2020
- [ ] 01/11/2020
- [ ] 01/13/2020

### Program Increment 3
- [ ] 01/15/2020
- [x] 01/18/2020
- [x] 01/22/2020
- [ ] 01/18/2020
- [ ] 01/25/2020
- [ ] 01/27/2020
- [ ] 01/20/2020
- [ ] 02/03/2020
- [ ] 02/05/2020
- [ ] 02/10/2020
- [ ] 02/12/2020
- [ ] 02/17/2020
- [ ] 02/19/2020
- [ ] 02/24/2020
- [ ] 03/02/2020
- [ ] 03/09/2020
- [ ] 03/11/2020
- [ ] 03/16/2020
- [ ] 03/18/2020
- [ ] 03/23/2020
- [ ] 03/25/2020
